# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

# Ruby On Rails Rails Project 

* Bu projede, kullanıcılar kitapları görüntüleyebilir, ekleyebilir, değiştirebilir ve kaldırabilirler. Ek olarak, kullanıcılar kendi alışveriş sepetlerini oluşturabilir ve sepetlerine kitaplar ekleyerek ödeme yapabilirler.

### Önkoşullar

- [Ruby](https://www.ruby-lang.org/en/) yüklü olmalıdır.
- [Rails](https://rubyonrails.org/) yüklü olmalıdır.
- [Bundler](https://bundler.io/) yüklü olmalıdır.
- [Git](https://git-scm.com/) yüklü olmalıdır.
- [PostgreSQL](https://www.postgresql.org/download/) yüklü olmalıdır.


## Kurulum Adımları:

* Proje dizinine gidin:

``` 
 cd onlineBookManagement
``` 

* Gerekli bağımlılıkları yükleyin:

``` 
 bundle install
``` 

* Veritabanını oluşturun ve migrate işlemlerini yapın:

``` 
rails db:create
rails db:migrate
``` 

* Sunucuyu başlatın:

``` 
rails server
``` 

* Şimdi, http://localhost:3000 adresine giderek projeyi görebilirsiniz.

## API Uç Noktaları

- Kullanıcı Kaydı:
  - Endpoint: POST /signup
  - Açıklama: Yeni kullanıcıları kaydetmek için kullanılır.

- Kullanıcı Girişi:
  - Endpoint: POST /login
  - Açıklama: Kullanıcıları giriş yapmak için kullanılır.

- Kitap Listesi:
  - Endpoint: GET /books
  - Açıklama: Tüm kitapları listelemek için kullanılır.

- Kitap Detayı:
  - Endpoint: GET /books/:id
  - Açıklama: Belirli bir kitabın detaylarını almak için kullanılır.

- Kitap Ekleme:
  - Endpoint: POST /books/create
  - Açıklama: Yeni kitapları eklemek için kullanılır.

- Kitap Güncelleme:
  - Endpoint: PUT /books/:id
  - Açıklama: Varolan bir kitabın bilgilerini güncellemek için kullanılır.

- Kitap Silme:
  - Endpoint: DELETE /books/:id
  - Açıklama: Varolan bir kitabı silmek için kullanılır.

## Kimlik Doğrulama

Bu uygulama, kullanıcıların kayıt olmaları ve giriş yapmaları gerektiğinden, kimlik doğrulama mekanizması gerektirir. Kullanıcıların hesaplarına erişim sağlamak için JWT (JSON Web Token) tabanlı bir kimlik doğrulama sistemi kullanılmaktadır.
