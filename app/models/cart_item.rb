class CartItem < ApplicationRecord
  belongs_to :cart
  belongs_to :book

  def calculate_subtotal
    self.subtotal = quantity * book.price
    Cart.total_price = cart_items.where(cart_id: self.cart_id).sum(:subtotal)
  end
end

