class Cart < ApplicationRecord
  belongs_to :user
  has_many :cart_items, dependent: :destroy

  def calculate_total_price
    #self.total_price = cart_items.sum(&:subtotal)
    self.total_price = cart_items.where(cart_id: self.cart_id).sum(:subtotal)

  end
end
